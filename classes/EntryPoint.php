<?php

class EntryPoint
{
    private $route;
    private $routes;
    private $method;

    public function __construct(string $route, string $method, EmployeeRoutes $routes)
    {
        $this->route = $route;
        $this->method = $method;
        $this->routes = $routes;
    }

    private function loadTemplate(string $templateFileName, array $vars = [])
    {
        extract($vars);
        ob_start();
        include __DIR__ . '/../templates/' . $templateFileName;
        return ob_get_clean();
    }

    public function run()
    {
        $routes = $this->routes->getRoutes();
        $controller = $routes[$this->route][$this->method]['controller'];
        $action = $routes[$this->route][$this->method]['action'];
        $auth = $this->routes->getAuthentication();
        $page = $controller->$action();
        $isLoginRequired = $routes[$this->route]["login"] ?? false;
        if ($isLoginRequired && !$auth->isLoggedIn()) {
            header("location: index.php?route=auth/login");
        }


        if (isset($routes[$this->route]['roles']) && !in_array($auth->getUser()['pozice'], $routes[$this->route]['roles'])) {
            echo $this->loadTemplate("displayError.html.php", ["error" => "Nemáte povolení provádět tuto akci"]);
            return;
        }
        $title = $page["title"];

        if (isset($page['vars'])) {
            $output = $this->loadTemplate($page['template'], $page['vars']);
        } else {
            $output = $this->loadTemplate($page['template']);
        }

        if (isset($page['css'])) {
            $cssPath = "./styles/" . $page['css'];
        }

        echo $this->loadTemplate("layout.html.php", [
            "title" => $title,
            "output" => $output,
            "isLoggedIn" => $auth->isLoggedIn(),
            "user" => $auth->getUser(),
            "css" => $cssPath ?? null
        ]);
    }
}
