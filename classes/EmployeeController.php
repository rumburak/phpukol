<?php
class EmployeeController
{
    //client 131062190477-7n95rips3hpuro8nj4otggkdluqobo8r.apps.googleusercontent.com
    //secret  13zGWs8ctd3nVe2PnvIVbHfJ
    private $employeeTable;
    private $supervisorTable;
    private $positionTable;
    private $authentication;
    private $errorTable;
    private $taskTable;
    private $reportTable;
    private $employeeTaskTable;
    const EMPLOYEES_PER_PAGE = 9;

    public function __construct(
        Database $employeeTable,
        Database $supervisorTable,
        Database $positionTable,
        Database $taskTable,
        Database $employeeTaskTable,
        Database $reportTable,
        Authentication $authentication
    ) {
        $this->employeeTable = $employeeTable;
        $this->supervisorTable = $supervisorTable;
        $this->positionTable = $positionTable;
        $this->taskTable = $taskTable;
        $this->employeeTaskTable = $employeeTaskTable;
        $this->reportTable = $reportTable;
        $this->authentication = $authentication;
        $this->errorTable = [
            "jmeno" => "Zaměstnanec musí mít jméno",
            "prijmeni" => "Zaměstnanec musí mít příjmení",
            "pohlavi" => "Zaměstnanec musí mít pohlaví?",
            "ulice" => "Zaměstnanec musí mít Ulici",
            "obec" => "Zaměstnanec musí mít Obec",
            "telefon" => "Zaměstnanec musí mít telefon",
            "psc" => "Zaměstnanec musí mít PSČ",
            "email" => "Zaměstnanec musí mít mail",
            "pozice" => "Zaměstnanec musí mít pozici",
            "vychoziHeslo" => "Jako administrátor musíte nastavit výchozí heslo",
            "heslo" => "heslo nemůže být prázdné",
            "hesloPotvrzeni" => "Prosím, potvrtďte Vaše heslo",






        ];
    }


    public function home()
    {
        $page = $_GET['page'] ?? 1;
        $page = htmlspecialchars($page, ENT_QUOTES, 'UTF-8');
        $supervisors = $this->supervisorTable->findAllAndReturnData();
        $positions = $this->positionTable->findAllAndReturnData();
        $offset = ($page - 1)  * EmployeeController::EMPLOYEES_PER_PAGE;
        $user = $this->authentication->getUser();
        //prázndý string znamemá, že se nebude podle daného parametru třídit, nebo řadit
        $supervisor = '';
        $position = '';
        $sort = '';
        if (isset($_GET['sort']) && !empty($_GET['sort'])) {
            $sort = htmlspecialchars($_GET['sort'], ENT_QUOTES, 'UTF-8');
        }
        // } else {
        //     $allEmployees = $this->employeeTable->findAll();
        // }
        if (isset($_GET['supervisor'])  && !empty($_GET['supervisor'])) {
            $supervisor = htmlspecialchars($_GET['supervisor'], ENT_QUOTES, 'UTF-8');
        }

        if (isset($_GET['position'])  && !empty($_GET['position'])) {
            $position = htmlspecialchars($_GET['position'], ENT_QUOTES, 'UTF-8');
        }

        $employees = $this->employeeTable
            ->findAll()
            ->where("nadrizeny", $supervisor)
            ->and("pozice", $position)
            ->sort($sort)
            ->limit($offset, EmployeeController::EMPLOYEES_PER_PAGE)
            ->queryTest()
            ->fetchAll();

        $employees = array_map(function ($employee) use ($user) {
            return $this->addPermmissionForRole($employee, $user);
        }, $employees);

        $count = $this->employeeTable
            ->countAll()
            ->where("nadrizeny", $supervisor)
            ->and("pozice", $position)
            ->queryTest()
            ->fetch()[0];
        $maxPage = ceil($count / EmployeeController::EMPLOYEES_PER_PAGE);
        $title = "Homepage";
        return [
            "title" => $title,
            "template" => "list.html.php",
            "vars" => [
                "employees" => $employees,
                "supervisors" => $supervisors,
                "page" => $page,
                "maxPage" => $maxPage,
                "positions" => $positions,
                "user" => $user
            ]
        ];
    }

    public function editOrAddNew()
    {
        $user = $this->authentication->getUser();
        $employee = null;
        $supervisors = $this->supervisorTable->findAllAndReturnData();
        $positions = $this->positionTable->findAllAndReturnData();
        if (isset($_GET['id'])) {
            $employee = $this->employeeTable->findById($_GET['id']);
            $employee = $this->addPermmissionForRole($employee, $user);
        }
        $title = "Editace Zaměstnance";
        return [
            "title" => $title,
            "template" => "editOrAddNew.html.php",
            "vars" => [
                "employee" => $employee,
                "supervisors" => $supervisors,
                "positions" => $positions,
                "user" => $user
            ]
        ];
    }

    public function addNew()
    {

        $user = $this->authentication->getUser();
        $employee = null;
        $supervisors = $this->supervisorTable->findAllAndReturnData();
        $positions = $this->positionTable->findAllAndReturnData();
        $title = "Přidání nového zaměstnance";
        return [
            "title" => $title,
            "template" => "addNew.html.php",
            "vars" => [
                "employee" => $employee,
                "supervisors" => $supervisors,
                "positions" => $positions,
                "user" => $user
            ]
        ];
    }
    // metoda umožňuje editaci i založení nového zaměstnance
    // vyhneme se tam vyvoření dou víceměně stejných metod a templátů

    //po rozšíření funkcionalit bylo nutné přidat metdody zvlášť pro editace a pro ukládání nových zaměstnanců

    public function save()
    {
        $positions = $this->positionTable->findAllAndReturnData();
        $user = $this->authentication->getUser();
        $employee = $_POST["emp"];
        $sanitizedEmp = [];
        foreach ($employee as $key => $value) {
            $sanitizedEmp[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        }
        $savedEmployee = $this->employeeTable->findById($sanitizedEmp['id']);
        if ($employee['posledni_zmena'] != $savedEmployee['posledni_zmena']) {
            $supervisors = $this->supervisorTable->findAllAndReturnData();
            $template = $sanitizedEmp["id"] != null ? "editOrAddNew.html.php" : "addNew.html.php";
            $savedEmployee = $this->addPermmissionForRole($savedEmployee, $user);
            return [
                "template" => $template,
                "title" => "Nový Zaměstnanec haah",
                "vars" => [
                    "errors" => ["Zaměstnanec byl změněn. Prosím zkuste editaci znovu"],
                    "employee" => $savedEmployee,
                    "supervisors" => $supervisors,
                    "user" => $user,
                    "positions" => $positions
                ]
            ];
        }

        $errors = [];
        foreach ($sanitizedEmp as $key => $value) {
            if (empty($value) && isset($this->errorTable[$key])) {
                $errors[] = $this->errorTable[$key];
            }
        }

        if ($sanitizedEmp["pozice"] == "dělník" && empty($sanitizedEmp["nadrizeny"])) {
            $errors[] = "pro pozici dělník musíte musí mít nadřízeného";
        }
        if (empty($errors)) {
            if ($sanitizedEmp['nadrizeny'] == '') {
                $sanitizedEmp['nadrizeny'] = null;
            }

            $sanitizedEmp['posledni_zmena'] = date("Y-m-d H:i:s");

            //pokud byl uživatel nově zakládán, tak by byla chyba zachycena dříve
            // pokud v tomto bodě neexistuje heslo, tak to znamená, že je uživatel editiván
            if (!empty($sanitizedEmp["heslo"])) {
                $sanitizedEmp['heslo'] = password_hash($sanitizedEmp['heslo'], PASSWORD_DEFAULT);
            }
            echo $sanitizedEmp['id'];
            $this->employeeTable->save($sanitizedEmp);

            if ($sanitizedEmp["pozice"] == "mistr") {
                $newEmployee = $this->employeeTable->findOne("email", $sanitizedEmp["email"]);
                $id = $newEmployee["id"];
                $firstname = $newEmployee["jmeno"];
                $lastname = $newEmployee["prijmeni"];
                $fullname = $firstname . " " . $lastname;
                $this->supervisorTable->save(["jmeno" => $fullname, "id_zamestnance" => $id, "id" => null]);
            }
            header("location: index.php?route=employees/home");
        } else {
            $supervisors = $this->supervisorTable->findAllAndReturnData();
            $template = $sanitizedEmp["id"] != null ? "editOrAddNew.html.php" : "addNew.html.php";
            $employee = $this->addPermmissionForRole($employee, $user);
            return [
                "template" => $template,
                "title" => "Nový Zaměstnanec haah",
                "vars" => [
                    "errors" => $errors,
                    "employee" => $employee,
                    "supervisors" => $supervisors,
                    "user" => $user,
                    "positions" => $positions
                ]
            ];
        }
    }

    public function yourEmployees()
    {
    }

    public function editPassword()
    {

        $title = "Změna hesla";
        $user = $this->authentication->getUser();

        return [
            "title" => $title,
            "css" => "changePassword.css",
            "template" => "changePassword.html.php",
            "vars" => [
                "user" => $user
            ]
        ];
    }

    public function savePassword()
    {
        $user = $this->authentication->getUser();
        //uživatel nemůže měnit heslo bez přihlášení
        $errors = [];
        if (empty($user)) {
            return;
        }

        $oldPassword = $_POST["oldPassword"] ?? '';
        if (!password_verify($oldPassword, $user["heslo"])) {
            $errors[] = "Zadali jste špatné původní heslo";
        }
        if (empty($_POST["newPassword"])) {
            $errors[] = "Nové heslo nemůže být prázdné";
        }
        if (empty($_POST["passwordConfirm"])) {
            $errors[] = "Potvzrení heslo nemůže být prázdné";
        }
        if (
            !empty($_POST["newPassword"]) && !empty($_POST["newPassword"]) &&
            $_POST["newPassword"] != $_POST["passwordConfirm"]
        ) {
            $errors[] = "Zadali jste odlišná hesla";
        }

        if (empty($errors)) {
            $password = htmlspecialchars($_POST["newPassword"], ENT_QUOTES);
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
            $this->employeeTable->save(["id" => $user["id"], "heslo" => $hashedPassword]);
            $message = "Heslo úspěšně změněno";
            $title = "success";
            return [
                "title" => $title,
                "css" => "changePassword.css",
                "template" => "success.html.php",
                "vars" => [
                    "message" => $message
                ]
            ];
        } else {
            $title = "Změna hesla";
            return [
                "title" => $title,
                "template" => "changePassword.html.php",
                "css" => "changePassword.css",
                "vars" => [
                    "user" => $user,
                    "errors" => $errors
                ]
            ];
        }
    }

    private function addPermmissionForRole($employee, $user)
    {
        if (!$user) {
            $employee["editable"] = false;
            return $employee;
        }
        if ($user["id"] == $employee["id"]) {
            $employee["editable"] = true;
            $employee["editableFields"] = ["jmeno", "prijmeni", "pohlavi", "obec", "telefon", "email", "ulice", "psc",];
        } else if ($user["pozice"] == "admin") {
            $employee["editable"] = true;
            $employee["editableFields"] = ["pozice", "nadrizeny"];
        } else if ($user["pozice"] == "mistr" && $user["jmeno"] . " " . $user["prijmeni"] == $employee["nadrizeny"]) {
            $employee["editable"] = true;
            $employee["editableFields"] = ["pozice"];
        } else {
            $employee["editable"] = false;
        }

        return $employee;
    }

    public function employeeReport()
    {
        $user = $this->authentication->getUser();
        $employees = [];

        if ($user['pozice'] == "delnik") {
            return [
                "title" => "Akce zamítnuta",
                "template" => 'displayError.html.php',
                "vars" => ["error" => "Nemáte oprávnění pro tuto operaci"]
            ];
        }

        if ($user["pozice"] == "mistr") {
            $supervizor = $this->supervisorTable
                ->findOne("id_zamestnance", $user['id']);
            $employees = $this->employeeTable
                ->findAll()
                ->where("nadrizeny", $supervizor['id'])
                ->queryTest()
                ->fetchAll();
        } else {
            $employees = $this->employeeTable
                ->findAll()
                ->where("pozice", "delnik")
                ->queryTest()
                ->fetchAll();
        }

        return [
            "title" => "Vyhledání reportů",
            "template" => "findReport.html.php",
            "vars" => [
                "message" => "Soubor nená validní CSV formát",
                "employees" => $employees
            ]

        ];
    }

    public function findEmployeeReport()
    {
        $user = $this->authentication->getUser();
        $data = htmlspecialchars($_POST['zamestnanec'], ENT_QUOTES);
        $supervisor = $this->supervisorTable->findOne("id_zamestnance", $user['id']);
        $employee = $this->employeeTable->findById($data);
        if (!$employee) {

            return [
                "title" => "Akce zamítnuta",
                "template" => 'displayError.html.php',
                "vars" => ["error" => "Uživatel nebyl nalezen"]
            ];
        }

        if ($user["pozice"] == "mistr" && $employee['nadrizeny'] != $supervisor['id']) {
            return [
                "title" => "Akce zamítnuta",
                "template" => 'displayError.html.php',
                "vars" => ["error" => "Nemáte oprávnění zobrazit report tohoto zaměstnance"]
            ];
        }

        $reports = $this->reportTable
            ->findAll()
            ->where("zamestnanec_id", $employee['id'])
            ->queryTest()
            ->fetchAll();

        $statistics = [];
        $totalHours = 0;
        foreach ($reports as $report) {
            $totalHours += intval($report["pocet_hodin"]);
        }



        $reports = array_map(function ($report) {
            return $this->addTaskDataToReport($report);
        }, $reports);

        return [
            "title" => "Report zaměstnance",
            "template" => 'reportOverview.html.php',
            "css" => "reportOverview.css",
            "vars" => [
                "employee" => $employee,
                "reports" => $reports,
                "totalHours" => $totalHours,
                "user" => $user
            ]
        ];
    }

    private function addTaskDataToReport($report)
    {
        $id = $report["ukol_id"];
        $task = $this->taskTable->findById($id);
        $report["nazev_ukolu"] = $task["nazev"];
        $report["zatez"] = $task["pracovni_zatez"];
        return $report;
    }


    public function newTask()
    {
        $user = $this->authentication->getUser();
        $employees_select = [];
        $supervisor = $this->supervisorTable
            ->findAll()
            ->where("id_zamestnance", $user['id'])
            ->queryTest()
            ->fetch();
        if ($user['pozice'] == "mistr") {
            $employees_select = $this->employeeTable
                ->findAll()
                ->where("nadrizeny", $supervisor['id'])
                ->queryTest()
                ->fetchAll();
        }
        return [
            "title" => "Nový úkol",
            "template" => "newTask.html.php",
            "css" => "newTask.css",
            "vars" => [
                "message" => "Soubor nená validní CSV formát",
                "user" => $user,
                "employees" => $employees_select,
                "super" => $supervisor,
            ]

        ];
    }

    public function saveNewTask()
    {
        $user = $this->authentication->getUser();
        $employees_select = [];
        $supervisor = $this->supervisorTable
            ->findAll()
            ->where("id_zamestnance", $user['id'])
            ->queryTest()
            ->fetch();
        if ($user['pozice'] == "mistr") {
            $employees_select = $this->employeeTable
                ->findAll()
                ->where("nadrizeny", $supervisor['id'])
                ->queryTest()
                ->fetchAll();
        }
        $errors = [];
        foreach ($_POST as $key => $value) {
            # code...
            if (empty($_POST[$key]) && $key != "employee") {
                $errors[] = $key . " nemůže být prádné";
            }
        }
        if (!empty($errors)) {

            return [
                "title" => "Nový úkol",
                "template" => "newTask.html.php",
                "css" => "newTask.css",
                "vars" => [
                    "user" => $user,
                    "employees" => $employees_select,
                    "messages" => $errors,
                ]

            ];
        }
        $data = [];
        $supervisor = $this->supervisorTable
            ->findAll()
            ->where("id_zamestnance", $user['id'])
            ->queryTest()
            ->fetch();
        $data["nazev"] = htmlspecialchars($_POST["name"], ENT_QUOTES);
        $data["pracovni_zatez"] = htmlspecialchars($_POST["duration"], ENT_QUOTES);
        $data["zamestnanec_id"] = htmlspecialchars($_POST["employee"], ENT_QUOTES);
        $data["popis"] = htmlspecialchars($_POST["description"], ENT_QUOTES);
        $data["id"] = '';
        $data["nadrizeny_id"] = $supervisor['id'];
        $this->taskTable->save($data);
        $this->employeeTaskTable->save([
            "id_zamestnance" => $data['zamestnanec_id'],
            "id_ukol" => $this->taskTable->lastInsertedId()
        ]);
        return [
            "title" => "Nový úkol",
            "template" => "success.html.php",
            "vars" => [
                "message" => "Úkol byl založen"
            ]

        ];
    }

    public function taskList()
    {
        $user = $this->authentication->getUser();
        $idData = [];
        $choosenTasks = $this->employeeTaskTable
            ->findAll(["id_ukol"])
            ->where("id_zamestnance", $user['id'])
            ->queryTest()
            ->fetchAll();

        foreach ($choosenTasks as $task) {
            $idData[] = $task["id_ukol"];
        }

        $reports = $this->reportTable
            ->findAll()
            ->where("zamestnanec_id", $user['id'])
            ->queryTest()
            ->fetchAll();

        $user = $this->authentication->getUser();
        $tasks = $this->taskTable
            ->findAll()
            ->whereIn("id", $idData)
            ->queryTest()
            ->fetchAll();

        $tasks = $this->addProgressToTask($tasks, $reports);

        return [
            "title" => "Vaše úkoly",
            "template" => "taskList.html.php",
            "css" => "taskList.css",
            "vars" => [
                "user" => $user,
                "tasks" => $tasks,
            ]

        ];
    }

    private function addProgressToTask($tasks, $reports)
    {
        $taskProgress = [];
        $tt = [];
        foreach ($reports as $report) {
            if (isset($taskProgress[$report['ukol_id']])) {
                $taskProgress[$report['ukol_id']] += intval($report['pocet_hodin']);
            } else {
                $taskProgress[$report['ukol_id']] = intval($report['pocet_hodin']);
            }
        }

        foreach ($tasks as $task) {
            # code...
            $progress = ceil(((($taskProgress[$task['id']] ?? 0)  / intval($task['pracovni_zatez']))) * 100);
            $task["progres"] = $progress;
            $tt[] = $task;
        }


        return $tt;
    }

    public function chooseTask()
    {
        $user = $this->authentication->getUser();
        $idData = [];
        $messages = [];

        if (isset($_GET['taskId'])) {
            $task = $this->taskTable->findById($_GET['taskId']);
            if ($user["nadrizeny"] != $task['nadrizeny_id']) {
                $messages[] = "Tento úkol nevypsal váš nadřízený";
            }
            $isTaskChoosen = $this->employeeTaskTable
                ->findAll()
                ->where("id_zamestnance", $user["id"])
                ->and("id_ukol", $task['id'])
                ->queryTest()
                ->fetchAll();
            if (!empty($isTaskChoosen)) {
                $messages[] = "Tento úkol máte již vybraný";
            }

            if (empty($messages)) {
                $data = [];
                $data["id_zamestnance"] = $user['id'];
                $data['id_ukol'] = $task['id'];
                $this->employeeTaskTable->save($data);
            }
        }

        $choosenTasks = $this->employeeTaskTable
            ->findAll(["id_ukol"])
            ->where("id_zamestnance", $user['id'])
            ->queryTest()
            ->fetchAll();

        foreach ($choosenTasks as $task) {
            $idData[] = $task["id_ukol"];
        }
        $tasks = $this->taskTable
            ->findAll()
            ->whereIn("id", $idData, true)
            ->and("nadrizeny_id", $user['nadrizeny'])
            ->queryTest()
            ->fetchAll();

        return [
            "title" => "Výběr práce",
            "template" => "chooseTask.html.php",
            "css" => "taskList.css",
            "vars" => [
                "user" => $user,
                "tasks" => $tasks,
                "messages" => $messages
            ]

        ];
    }

    public function taskReport()
    {

        $user = $this->authentication->getUser();
        $task = $this->taskTable->findById($_GET["taskId"]);
        if ($task == null) {

            return [
                "title" => "Výkaz práce",
                "template" => "displayError.html.php",
                "css" => "taskList.css",
                "vars" => [
                    "error" => "Úkol s tímto ID neexistuje"
                ]

            ];
        }
        $hasPermission = true;
        $hasTask = $this->employeeTaskTable
            ->findAll()
            ->where("id_zamestnance", $user['id'])
            ->and("id_ukol", $task['id'])
            ->queryTest()
            ->fetchAll();
        if (empty($hasTask)) {
            $hasPermission = false;
        }

        return [
            "title" => "Výkaz práce",
            "template" => "taskReport.html.php",
            "css" => "taskList.css",
            "vars" => [
                "user" => $user,
                "task" => $task,
                "hasPermission" => $hasPermission
            ]

        ];
    }

    public function saveTaskReport()
    {
        $user = $this->authentication->getUser();
        $errors = [];
        $data = [];
        if (!isset($_POST['pocet_hodin']) || empty($_POST['pocet_hodin'])) {
            $errors[] = "Počet hodin musí být vyplněn";
        }

        if (!empty($errors)) {

            return [
                "title" => "Výkaz práce",
                "template" => "taskReport.html.php",
                "css" => "taskList.css",
                "vars" => [
                    "user" => $user,
                    "task" => $_POST,
                    "hasPermission" => true,
                    "messages" => $errors
                ]

            ];
        }

        foreach ($_POST as $key => $value) {
            $data[$key] = htmlspecialchars($value, ENT_QUOTES);
        }

        $data["zamestnanec_id"] = $user["id"];
        $data["id"] = '';

        $this->reportTable->save($data);
        return [
            "title" => "Práce vykázána",
            "template" => "displayError.html.php",
            "css" => "taskList.css",
            "vars" => [
                "error" => "Práce byla úspěšně vykázána",
                "link" => "index.php?route=employee/taskList"

            ]

        ];
    }
}
