<?php
class AdminController
{

   private $authentication;
   private $employeeTable;

   public function __construct(Authentication $authentication, Database $employeeTable)
   {
      $this->authentication = $authentication;
      $this->employeeTable = $employeeTable;
   }


   public function importEmployee()
   {

      $user = $this->authentication->getUser();

      return [
         "title" => "Import",
         "template" => "importEmployees.html.php",
         "user" => $user,

      ];
   }

   public function saveImportedEmployees()
   {
      $user = $this->authentication->getUser();
      if (isset($_FILES["userfile"])) {

         $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
         if (in_array($_FILES['userfile']['type'], $mimes)) {

            $csv = [];
            $file = fopen($_FILES["userfile"]["tmp_name"], 'r');
            $header =  fgetcsv($file, 0, ";");
            while (($line = fgetcsv($file, 0, ";")) !== FALSE) {
               $entry = array_combine($header, $line);
               $entry['id'] = '';
               if ($entry['nadrizeny'] == '') {
                  $entry['nadrizeny'] = null;
               }
               $this->employeeTable->save($entry);
               $csv[] = $entry;
            }
            fclose($file);

            return [
               "title" => "Import",
               "template" => "importEmployees.html.php",
               "vars" => [
                  "user" => $user,
                  "data" => $csv,
                  "message" => "Zaměstnanci byli úspěšně importováni"
               ]

            ];
            // do something
         } else {
            return [
               "title" => "Import",
               "template" => "importEmployees.html.php",
               "vars" => [
                  "user" => $user,
                  "message" => "Soubor nemá validní CSV formát"
               ]

            ];
         }
      }

      return [
         "title" => "Import",
         "template" => "importEmployees.html.php",
         "vars" => [
            "user" => $user,
            "message" => "Soubor nebyl nahrán"
         ]

      ];
   }
}
