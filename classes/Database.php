<?php
class Database
{
    private $pdo;
    private $table;
    private $primaryKey;
    private $queryString;
    private $parameters;

    public function __construct(PDO $pdo, string $table, string $primaryKey)
    {
        $this->pdo = $pdo;
        $this->table = $table;
        $this->primaryKey = $primaryKey;
        $this->queryString = "";
        $this->parameters = [];
    }


    public function lastInsertedId()
    {
        return $this->pdo->lastInsertId();
    }


    private function query(string $sql, array $params = [])
    {
        $preparedQuery = $this->pdo->prepare($sql);
        foreach ($params as $name => $value) {
            $preparedQuery->bindValue($name, $value);
        }
        $preparedQuery->execute();
        return $preparedQuery;
    }

    public function queryTest()
    {
        $preparedQuery = $this->pdo->prepare($this->queryString);
        foreach ($this->parameters as $name => $value) {
            $preparedQuery->bindValue($name, $value);
        }
        $preparedQuery->execute();
        $this->queryString = '';
        $this->parameters = [];
        return $preparedQuery;
    }

    public function findById(string $id)
    {
        $query = "SELECT * from " . $this->table . " WHERE " . $this->primaryKey . " = :id";
        return $this->query($query, [":id" => $id])->fetch();
    }

    public function findOne($column, $value)
    {
        $query = "SELECT * from " . $this->table . " WHERE " . $column . "  = :value";
        return $this->query($query, [":value" => $value])->fetch();
    }

    public function findAllAndReturnData(string $orderBy = '', string $order = "ASC")
    {
        $query = "select * FROM " . $this->table;
        if ($orderBy != '') {
            $query .= " ORDER BY " . $orderBy . " " . $order;
        }
        return $this->query($query)->fetchAll();
    }

    public function findAll($columns = [])
    {
        if (empty($columns)) {
            $this->queryString .= "select * FROM " . $this->table . " ";
            return $this;
        } else {
            $this->queryString .= "SELECT" . " ";
            foreach ($columns as $column) {
                $this->queryString .= " " . $column . ", ";
            }
            $this->queryString = trim($this->queryString);
            $this->queryString = rtrim($this->queryString, ',');
            $this->queryString .= " FROM " . $this->table . " ";
            return $this;
        }
    }

    public function countAll()
    {
        $this->queryString .= "select count(*) FROM " . $this->table . " ";
        return $this;
    }

    //metoda umožnňující řetězení metod, tak aby šlo např. vytvořit dotaz ... where ... order by ...
    public function sort(string $orderBy, string $order = "ASC"): Database
    {
        if ($orderBy == '') {
            return $this;
        }
        $this->queryString .= " ORDER BY " . $orderBy . " " . $order . " ";
        return $this;
    }

    //metoda umožnňující řetězení metod, tak aby šlo např. vytvořit dotaz ... where ... order by ...
    public function where($field, $value)
    {
        if ($value == '') {
            //vrácí vše proto, aby správně fungovala metoda and()
            $this->queryString .= " WHERE 1=1 ";
            return $this;
        }
        $this->queryString .= " WHERE " . $field . " = :value";
        $this->parameters[":value"] = $value;
        return $this;
    }

    public function whereIn($field, $values, $not = false)
    {

        if (empty($values)) {
            //vrácí vše proto, aby správně fungovala metoda and()
            $this->queryString .= " WHERE 1=1 ";
            return $this;
        }
        if ($not == true) {
            $this->queryString .= " WHERE " . $field . " NOT IN (";
            foreach ($values as $value) {
                $this->queryString .=   $value . ", ";
            }
        } else {
            $this->queryString .= " WHERE " . $field . " IN (";
            foreach ($values as $value) {
                $this->queryString .=   $value . ", ";
            }
        }
        $this->queryString = trim($this->queryString);
        $this->queryString = rtrim($this->queryString, ',');
        $this->queryString .= ") ";
        return $this;
    }

    public function and($field, $value)
    {
        if ($value == '') {
            return $this;
        }
        $this->queryString .= " AND " . $field . " = :andParam";
        $this->parameters[":andParam"] = $value;
        return $this;
    }

    public function findAllWhere(string $field, string $value)
    {

        $query = "select * FROM " . $this->table;
        $query .= " WHERE " . $field . " = :value";
        $parameter = [":value" => $value];
        return $this->query($query, $parameter)->fetchAll();
    }

    public function insert(array $data)
    {
        $query = "INSERT INTO " . $this->table . "(";
        foreach ($data as $key => $value) {
            $query .=  $key . ',';
        }
        // odstraneni posledni prebytecne carky
        $query = rtrim($query, ',');
        $query .= ") VALUES (";
        foreach ($data as $key => $value) {
            $query .= ':' . $key . ',';
        }
        $query = rtrim($query, ',');
        $query .= ')';
        $this->query($query, $data);
    }

    public function update($fields)
    {
        $query = 'UPDATE ' . $this->table . ' SET ';
        foreach ($fields as $key => $value) {
            $query .= '`' . $key . '` = :' . $key . ',';
        }
        $query = rtrim($query, ',');
        $query .= ' WHERE `' . $this->primaryKey . '` = :id';

        $fields[':id'] = $fields['id'];

        $this->query($query, $fields);
    }

    public function limit($offset, $results)
    {
        $this->queryString .= " LIMIT " . $offset . ", " . $results;
        return $this;
    }

    public function count()
    {
        return $this->query("SELECT count(*) FROM " . $this->table)->fetch();
    }


    public function save(array $data)
    {
        //při vytvoření nového zaměstnance je id nastaveno ve formě na prázdný string
        //přenastavení na null, aby došlo v db k auto increment
        if (!isset($data['id'])) {
            $this->insert($data);
        } else if ($data['id'] == '' || $data['id'] == null) {
            $data['id'] = null;
            $this->insert($data);
        } else {
            $this->update($data);
        }
    }
}
