<?php


class EmployeeRoutes
{
    private  $employeeTable;
    private  $supervisorTable;
    private  $positionTable;
    private  $taskTable;
    private  $taskEmployeeTable;
    private  $reportTable;
    private  $authentication;

    public function __construct()
    {
        global $CLIENT_ID;
        global $CLIENT_SECRET;
        global $REDIRECT_URL;
        include __DIR__ . "/../includes/dbConnection.php";
        $this->employeeTable =  new Database($pdo, "zamestnanec", "id");
        $this->supervisorTable =  new Database($pdo, "nadrizeny", "jmeno");
        $this->positionTable = new Database($pdo, "pozice", "id");
        $this->taskTable = new Database($pdo, 'ukol', "id");
        $this->reportTable = new Database($pdo, "report", "id");
        $this->taskEmployeeTable = new Database($pdo, 'ukol_zamestnanec', 'id');


        $this->authentication = new Authentication($this->employeeTable);
    }

    public function getRoutes()
    {
        $employeeController = new EmployeeController(
            $this->employeeTable,
            $this->supervisorTable,
            $this->positionTable,
            $this->taskTable,
            $this->taskEmployeeTable,
            $this->reportTable,
            $this->authentication
        );
        $authController = new AuthController($this->authentication);


        $adminController = new AdminController($this->authentication, $this->employeeTable);

        $rotues = [
            "employees/home" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "home"
                ],

            ],

            "employee/edit" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "editOrAddNew"
                ],
                "POST" => [
                    "controller" => $employeeController,
                    "action" => "save"
                ],
                "login" => true

            ],

            "employee/addNew" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "addNew"
                ],

                "POST" => [
                    "controller" => $employeeController,
                    "action" => "save"
                ],

                "roles" => ["admin"],
                "login" => true

            ],

            "employee/changePassword" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "editPassword"
                ],
                "POST" => [
                    "controller" => $employeeController,
                    "action" => "savePassword"
                ],
                "login" => true,
                "roles" => ["admin", "mistr", "dělník"]
            ],

            "auth/login" => [
                "GET" => [
                    "controller" => $authController,
                    "action" => "showLoginForm"
                ],
                "POST" => [
                    "controller" => $authController,
                    "action" => "login"
                ]
            ],
            "auth/logout" => [
                "GET" => [
                    "controller" => $authController,
                    "action" => "logout"
                ],
                "login" => true
            ],

            "oauth/callback" => [
                "GET" => [
                    "controller" => $authController,
                    "action" => "loginWithGoogle"
                ]
            ],

            "findReport" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "employeeReport"
                ],
                "POST" => [
                    "controller" => $employeeController,
                    "action" => "findEmployeeReport"
                ],
                "roles" => ["admin", "mistr"]
            ],


            "employee/newTask" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "newTask"
                ],
                "POST" => [
                    "controller" => $employeeController,
                    "action" => "saveNewTask"
                ],
                "login" => true,
                "roles" => ["mistr"]
            ],

            "employee/taskList" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "taskList"
                ],
                "login" => true,
                "roles" => ["dělník"]
            ],

            "employee/chooseTask" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "chooseTask"
                ],
                "login" => true
            ],

            "employee/report" => [
                "GET" => [
                    "controller" => $employeeController,
                    "action" => "taskReport"
                ],
                "POST" => [
                    "controller" => $employeeController,
                    "action" => "saveTaskReport"
                ],
                "login" => true,
                "roles" => ["dělník"]
            ],


            "admin/importEmployee" => [
                "GET" => [
                    "controller" => $adminController,
                    "action" => "importEmployee"
                ],

                "POST" => [
                    "controller" => $adminController,
                    "action" => "saveImportedEmployees"
                ],
                "login" => true,
                "roles" => ["admin"]
            ]
        ];

        return $rotues;
    }

    public function getAuthentication()
    {
        return $this->authentication;
    }
}
