<?php
require_once __DIR__ . "/../oauthConfig.php";
require_once __DIR__  . '/../vendor/autoload.php';

class AuthController
{

   private  $authentication;
   private $client;
   const clientId = CLIENT_ID;
   const clientSecret = CLIENT_SECRET;
   const redirectURL = REDIRECT_URL;


   public  function __construct(Authentication $authentication)
   {
      $this->authentication = $authentication;
      $this->client = new Google_Client();
      $this->client->setClientId(AuthController::clientId);
      $this->client->setClientSecret(AuthController::clientSecret);
      $this->client->setRedirectUri(AuthController::redirectURL);
      $this->client->addScope("email");
      $this->client->addScope("profile");
   }

   public function showLoginForm()
   {
      $redirect = $this->client->createAuthUrl();
      return ["title" => "přihlášení", "template" => 'loginForm.html.php', "vars" => ["redirect" => $redirect]];
   }

   public function login()
   {
      if (!empty($_POST['email']) && !empty($_POST["password"]) &&  $this->authentication->login($_POST['email'], $_POST['password'])) {
         header("location: index.php?");
      } else {
         return ["title" => "přihlášení", "template" => 'loginForm.html.php', "vars" => ["error" => "chybně zadané údaje"]];
      }
   }

   public function loginWithGoogle()
   {
      if (isset($_GET['code'])) {
         $token = $this->client->fetchAccessTokenWithAuthCode($_GET['code']);
         $this->client->setAccessToken($token['access_token']);

         // get profile info
         $google_oauth = new Google_Service_Oauth2($this->client);
         $google_account_info = $google_oauth->userinfo->get();
         $email =  $google_account_info->email;
         $name =  $google_account_info->name;
         $name_arr = explode(" ", $name);
         $firstname = $name_arr[0];
         $lastname = $name_arr[1];
         echo $name;
         $this->authentication->loginWithGoogle($email, $firstname, $lastname);
         header("location: index.php?");


         // now you can use this profile info to create account in your website and make user logged in.
      } else {
         return ["title" => "přihlášení", "template" => 'loginForm.html.php', "vars" => ["error" => "Nastala chyba při přihlašováním s Googlem"]];
      }
   }

   public function logout()
   {
      session_destroy();
      header("location: index.php?");
   }
}
