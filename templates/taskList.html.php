<h1 class="title is-1 is-centered">Seznam Vaších Úkolů</h1>
<div class="tasklist">
   <?php foreach ($tasks as $task) : ?>
      <div class="element">
         <div class="card task mt-5 mb-5">
            <header class="card-header">
               <p class="card-header-title">
                  <?= $task["nazev"] ?>
               </p>
            </header>
            <div class="card-content">
               <div class="content">
                  <?php if ($task['progres'] > 100) : ?>
                     <div class="warning is-danger">

                        Máte vykázáno více hodin než je odhadovaná pracnost
                     </div>
                  <?php endif ?>
                  <?= $task["popis"] ?>
               </div>
            </div>

            <footer class="card-footer task-footer">

               <progress style="margin: 0%;" class=" <?= $task['progres'] > 100 ? "is-danger progress  is-small" : "is-success progress  is-small" ?>" value="<?= $task['progres'] ?>" max="100">60%</progress>
               <a href="index.php?route=employee/report&taskId=<?= $task['id'] ?>" class="card-footer-item btn">Vykázat Práci</a>
            </footer>
         </div>
      </div>
   <?php endforeach; ?>
</div>