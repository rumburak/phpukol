<h1 style="text-align: center;" class="title is-h1">Přihlášení</h1>
<?php if (isset($error)) : ?>
   <span><?= $error ?></span>
<?php endif ?>

<form style="width: 50%; margin: 0 auto;" action="" method="POST">
   <div class="field">
      <label class="label" for="email">Email</label>
      <input class="input is-link" type="text" name="email" id="email">
   </div>
   <div class="field">
      <label class="label" for="password">Heslo</label>
      <input class="input is-link" type="password" name="password" id="password">
   </div>
   <input class="button is-link" type="submit" value="Přihlásit se">
   <a class="button is-black mt-1" href="<?= $redirect ?>">
      <span class="icon-text">
         <span class="icon">
            <i class="fab fa-google"></i>
         </span>
         <span>Přihlášení s Googlem</span>
      </span>
   </a>
</form>

<!-- https://www.google.com/imgres?imgurl=https%3A%2F%2Fimg-authors.flaticon.com%2Fgoogle.jpg&imgrefurl=https%3A%2F%2Fwww.flaticon.com%2Fauthors%2Fgoogle&tbnid=r7jpcM1WRW_P8M&vet=12ahUKEwidspuCvdbwAhWQBhoKHeg_DA0QMygAegUIARDXAQ..i&docid=KKReSPDy_9t4xM&w=300&h=300&q=google%20icon&ved=2ahUKEwidspuCvdbwAhWQBhoKHeg_DA0QMygAegUIARDXAQ -->