<?php if ($employee["editable"]) : ?>
    <?php if (!empty($errors)) : ?>
        <div class="errors">
            <p>Tyto pole obsahují chybu</p>
            <ul>
                <?php
                foreach ($errors as $error) :
                ?>
                    <li><?= $error ?></li>
                <?php
                endforeach; ?>
            </ul>
        </div>
    <?php
    endif;
    ?>

    <?= $employee["jmeno"] ?>
    <form class="mb-5" style="width: 70%; margin: 0 auto;" action="" method="POST">
        <input type="hidden" name="emp[id]" value="<?= $employee['id'] ?? '' ?>">
        <input type="hidden" name="emp[posledni_zmena]" value="<?= $employee['posledni_zmena'] ?? '' ?>">
        <div class="field">
            <label class="label" for="firstname">Jméno</label>
            <input class="input" <?= !in_array("jmeno", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[jmeno]" id="firstname" value="<?= $employee['jmeno'] ?? '' ?>">
        </div>

        <div class="field">
            <label class="label" for="lastname">Příjmení</label>
            <input class="input" <?= !in_array("prijmeni", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[prijmeni]" id="lastname" value="<?= $employee['prijmeni'] ?? '' ?>">
        </div>


        <div class="field">
            <label for="gender">Pohlaví</label>
            <select class="select" <?= !in_array("pohlavi", $employee["editableFields"]) ?  'disabled' : '' ?> name="emp[pohlavi]" id="gender">
                <option value="Muž">Muž</option>
                <option value="Žena">Žena</option>
                <option value="Agender">Agender</option>
                <option value="Transgender">Transgender</option>
                <option value="Vrtulník Apache">Vrtulník Apache</option>
            </select>
        </div>

        <div class="field">
            <label class="label" for="street">Ulice</label>
            <input class="input" <?= !in_array("ulice", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[ulice]" id="street" value="<?= $employee['ulice'] ?? '' ?>">
        </div>
        <div class="field">
            <label class="label" for="city">Obec</label>
            <input class="input" <?= !in_array("obec", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[obec]" id="city" value="<?= $employee['obec'] ?? '' ?>">
        </div>
        <div class="field">
            <label class="label" for="zipCode">PSČ</label>
            <input class="input" <?= !in_array("psc", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[psc]" id="zipCode" value="<?= $employee['psc'] ?? '' ?>">
        </div>


        <div class="field">
            <label class="label" for="telefon">Telefon</label>
            <input class="input" <?= !in_array("telefon", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[telefon]" id="phone" value="<?= $employee['telefon'] ?? '' ?>">
        </div>


        <div class="field">
            <label class="label" for="mail">E-Mail</label>
            <input class="input" <?= !in_array("email", $employee["editableFields"]) ?  'readonly' : '' ?> type="text" name="emp[email]" id="mail" value="<?= $employee['email'] ?? '' ?>">
        </div>



        <div class="field">
            <label class="label" for="position">Pozice</label>
            <?php if (in_array("pozice", $employee["editableFields"])) : ?>

                <select class="select" name="emp[pozice]" id="supervisor">
                    <option></option>
                    <?php foreach ($positions as $position) : ?>
                        <?php if ($employee['pozice'] == $position['nazev_pozice']) : ?>
                            <option selected value="<?= $position['nazev_pozice'] ?>"><?= $position['nazev_pozice'] ?></option>
                        <?php else : ?>
                            <option value="<?= $position['nazev_pozice'] ?>"><?= $position['nazev_pozice'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>

            <?php else : ?>
                <select class="select" name="emp[pozice]" id="supervisor">
                    <option></option>
                    <?php foreach ($positions as $position) : ?>
                        <?php if ($employee['pozice'] == $position['nazev_pozice']) : ?>
                            <option selected value="<?= $position['nazev_pozice'] ?>"><?= $position['nazev_pozice'] ?></option>
                        <?php else : ?>
                            <option disabled value="<?= $position['nazev_pozice'] ?>"><?= $position['nazev_pozice'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>

        </div>



        <div class="fiel">

            <label class="label" for="supervisor">Nadřízený</label>
            <?php if (in_array("nadrizeny", $employee["editableFields"])) : ?>
                <select class="select" name="emp[nadrizeny]" id="supervisor">
                    <option></option>
                    <?php foreach ($supervisors as $supervisor) : ?>
                        <?php if ($employee['nadrizeny'] == $supervisor['id']) : ?>
                            <option selected value="<?= $supervisor['id'] ?>"><?= $supervisor['jmeno'] ?></option>
                        <?php else : ?>
                            <option value="<?= $supervisor['id'] ?>"><?= $supervisor['jmeno'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            <?php else : ?>
                <select class="select" name="emp[nadrizeny]" id="supervisor">
                    <option></option>
                    <?php foreach ($supervisors as $supervisor) : ?>
                        <?php if ($employee['nadrizeny'] == $supervisor['id']) : ?>
                            <option selected value="<?= $supervisor['id'] ?>"><?= $supervisor['jmeno'] ?></option>
                        <?php else : ?>
                            <option disabled value="<?= $supervisor['id'] ?>"><?= $supervisor['jmeno'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
        </div>

        <div class="field is-grouped is-grouped-centered">
            <div class="control mt-1"> <a class="button is-danger" href="index.php">Zpět</a></div>
            <div class="control">
                <input class="button is-link" type="submit" value="Odeslat">
            </div>
        </div>
    </form>

<?php else : ?>
    <span>Můžete editovat pouze Vaše údaje</span>
<?php endif; ?>