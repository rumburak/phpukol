<div>
    <form action="" method="GET">
        <div class="block options">
            <div>
                <p>Vyhledat zaměstance podle nadřízeného</p>
                <div class="select is-small mt-1">

                    <select name="supervisor" id="supervisor">
                        <option></option>
                        <?php foreach ($supervisors as $supervisor) : ?>
                            <option value="<?= $supervisor['id'] ?>"><?= $supervisor['jmeno'] ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>

            <div>
                <p>Vyhledat zaměstance podle pozice</p>
                <div class="select is-small mt-1">

                    <select name="position" id="position">
                        <option></option>
                        <?php foreach ($positions as $position) : ?>
                            <option value="<?= $position['nazev_pozice'] ?>"><?= $position['nazev_pozice'] ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>

            <div>
                <p>Řazení zaměsntanců podle:</p>

                <div class="select is-small mt-1">
                    <select class="is-hovered" name="sort" id="sort">
                        <option></option>
                        <option value="jmeno">Jméno</option>
                        <option value="prijmeni">Příjmení</option>
                        <option value="email">E-Mail</option>
                    </select>
                </div>
            </div>
        </div>
        <div class=" buttons is-centered mb-2">
            <button class="button is-dark">Vyhledat</button>
        </div>
    </form>
</div>
<table class=" table is-bordered is-striped is-fullwidth">
    <tr>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>pohlavi</th>
        <th>Ulice</th>
        <th>Obec</th>
        <th>PSČ</th>
        <th>Telefon</th>
        <th>Email</th>
        <th>Pozice</th>
        <th>Nadřízený</th>
    </tr>
    <?php foreach ($employees as $employee) : ?>
        <tr>
            <td><?= $employee["jmeno"] ?></td>
            <td><?= $employee["prijmeni"] ?></td>
            <td><?= $employee["pohlavi"] ?></td>
            <td><?= $employee["ulice"] ?></td>
            <td><?= $employee["obec"] ?></td>
            <td><?= $employee["psc"] ?></td>
            <td><?= $employee["telefon"] ?></td>
            <td><?= $employee["email"] ?></td>
            <td><?= $employee["pozice"] ?></td>
            <td><?= $employee["nadrizeny"] ?></td>
            <td>

                <?php if ($employee["editable"]) : ?>

                    <a href="index.php?route=employee/edit&id=<?= $employee['id'] ?>">Editovat</a>
                <?php else : ?>
                    <span>Nelze editovat</span>
                <?php endif ?>
            </td>

        </tr>
    <?php endforeach; ?>

</table>


<nav class="pagination is-centered mb-5" role="navigation" aria-label="pagination">
    <?php if ($page > 1) : ?>
        <?php
        $query = $_GET;
        // replace parameter(s)
        $query['page'] = $page - 1;
        // rebuild url
        $query_result = http_build_query($query);
        // new link
        ?>
        <a href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" class="pagination-previous">Previous</a>
    <?php else : ?>
        <a href="#" class="pagination-previous is-link" disabled>Previous</a>
    <?php endif; ?>


    <?php if ($page < $maxPage) : ?>
        <?php
        $query = $_GET;
        // replace parameter(s)
        $query['page'] = $page + 1;
        // rebuild url
        $query_result = http_build_query($query);
        // new link
        ?>
        <a href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" class="pagination-next">Next page</a>
    <?php else : ?>
        <a href="#" class="pagination-next is-link" disabled>Next page</a>

    <?php endif; ?>
    <ul class="pagination-list">
        <li><a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=1" class="pagination-link <?= $page == 1 ? "is-current" : "" ?>" aria-label="Goto page <?= $maxPage ?>">1</a></li>
        <li><span class="pagination-ellipsis">&hellip;</span></li>


        <?php for ($i = 2; $i < $maxPage; $i++) : ?>
            <li><a href="<?php echo $_SERVER['PHP_SELF'] . "?page=" . $i; ?>" class="pagination-link <?= $page == $i ? "is-current" : "" ?>" aria-label="Goto page <?= $i ?>"><?= $i ?></a></li>
        <?php endfor ?>

        <li><span class="pagination-ellipsis">&hellip;</span></li>
        <li><a <a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=<?= $maxPage ?>" class="pagination-link <?= $page == $maxPage ? "is-current" : "" ?>" aria-label="Goto page 86"><?= $maxPage ?></a></li>
    </ul>
</nav>