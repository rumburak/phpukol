<h1 class="title is-h1">Nahrajte zaměstnance</h1>

<?php if (isset($message)) : ?>
   <div> <strong><?= $message ?></strong></div>
<?php endif ?>
<form enctype="multipart/form-data" method="POST">
   <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
   <div style="display: inline-block; margin-right: 20px; margin-top: 5px;" class="file has-name">
      <label class="file-label">
         <input class="file-input" type="file" name="userfile">
         <span class="file-cta">
            <span class="file-icon">
               <i class="fas fa-upload"></i>
            </span>
            <span class="file-label">
               Vyberte Soubor
            </span>
         </span>
         <span class="file-name">

         </span>
      </label>
   </div>
   <input class="button is-link" type="submit" value="Nahrát" />

</form>



<script>
   console.log(44);
   const fileInput = document.querySelector(".file-input");
   const fileName = document.querySelector(".file-name");
   fileInput.addEventListener('change', e => {
      console.log(e.target.value);
      fileName.textContent = e.target.value.split('\\')[2]
   })
</script>