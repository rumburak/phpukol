<div class="container">
   <h1 class="title is-h1">Nový úkol</h1>
   <?php if (isset($messages)) : ?>
      <ul>
         <?php foreach ($messages as $message) : ?>
            <li><?= $message ?></li>
         <?php endforeach; ?>
      </ul>
   <?php endif; ?>
   <form method="post">
      <div class="field">
         <label class="label" for="name">Název</label>

         <div class="control">
            <input class="input is-info" type="text" name="name" id="name">
         </div>
      </div>
      <div class="field">
         <label class="label" for="description">Popis</label>
         <div class="control">
            <textarea class="textarea is-info" name="description" id="description" cols="30" rows="6"></textarea>
         </div>
      </div>
      <div class="horizontal-fields">

         <div class="form-field">
            <label class="label form-label" for="duration">Odhadovaná pracovní zátěž v hodinách</label>
            <div class="control in-form">
               <input class="input is-info" name="duration" id="duration" type="number" min="1" max="100">
            </div>
         </div>

         <div class="form-field">
            <label class="label form-label" for="employee">Přiřadit zaměstnance</label>
            <select class="select is-info" name="employee" name="employee" id="employee">
               <option value=""></option>
               <?php foreach ($employees as $employee) : ?>
                  <option value="<?= $employee["id"] ?>"> <?= $employee["jmeno"] . " " . $employee["prijmeni"] ?> </option>
               <?php endforeach; ?>
            </select>

         </div>

      </div>

      <div class="field is-grouped is-grouped-centered">
         <div class="control">
            <a href="index.php" class="button is-link is-light">Zpět</a>
         </div>
         <div class="control">
            <input type="submit" class="button is-link" value="Odeslat" />
         </div>
      </div>
   </form>
</div>