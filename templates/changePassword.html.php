   <?php if (!empty($errors)) : ?>
      <div class="errors">
         <p>Tyto pole obsahují chybu</p>
         <ul>
            <?php
            foreach ($errors as $error) :
            ?>
               <li><?= $error ?></li>
            <?php
            endforeach; ?>
         </ul>

      </div>
   <?php
   endif;
   ?>
   <form action="" method="POST">
      <div class="field">
         <label class="label" for="oldPassword">Původní heslo</label>
         <input class="input" id="oldPassword" type="password" name="oldPassword">
      </div>

      <div class="field">
         <label class="label" for="newPassword">Nové heslo</label>
         <input class="input" id="newPassword" type="password" name="newPassword">
      </div>

      <div class="field">
         <label class="label" for="passwordConfirm">potvrtďte heslo</label>
         <input class="input" id="passwordConfirm" type="password" name="passwordConfirm">
      </div>

      <div class="field is-grouped is-grouped-centered">
         <div class="control mt-1"> <a class="button is-danger" href="index.php">Zpět</a></div>
         <div class="control">
            <input class="button is-link" type="submit" value="Změnit Heslo">
         </div>
      </div>
   </form>