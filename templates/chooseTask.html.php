<h1 class="title is-1">Vyberte si úkol z nabídky</h1>
<ul>
   <?php foreach ($messages as $message) : ?>
      <li><?= $message ?></li>
   <?php endforeach; ?>
</ul>
<div class="tasklist">
   <?php foreach ($tasks as $task) : ?>
      <div class="card task mt-5 mb-5">
         <header class="card-header">
            <p class="card-header-title">
               <?= $task["nazev"] ?>
            </p>
         </header>
         <div class="card-content">
            <div class="content">
               <?= $task["popis"] ?>
            </div>
         </div>
         <footer class="card-footer task-footer">
            <progress style="margin: 0%;" class="progress is-success is-small" value="0" max="100">60%</progress>
            <a href="index.php?route=employee/chooseTask&taskId=<?= $task['id'] ?>" class="card-footer-item btn">Přihlásit se</a>
         </footer>
      </div>
   <?php endforeach; ?>
</div>