<?php if (!isset($employee)) : ?>
   <h1>Užitel není vybrán</h1>
   <a href="index.php">Zpět </a>
<?php else : ?>
   <h1 class="title is-h1">Reporty uživatele: <span class="name"><?= $employee['jmeno'] . " " . $employee['prijmeni'] ?></span></h1>
   <table class="table">
      <thead>
         <th>Název Úkolu</th>
         <th>počet vykázaných hodin</th>
         <th>Odhadovaná pracovní zátěž</th>
         <th>Datun</th>
         <th>Poznámka</th>
      </thead>
      <tbody>
         <?php foreach ($reports as $report) : ?>
            <tr>
               <td><?= $report['nazev_ukolu'] ?></td>
               <td><?= $report["pocet_hodin"] ?></td>
               <td><?= $report['zatez'] ?></td>
               <td><?= $report['datum'] ?></td>
               <td><?= $report['poznamka'] ?></td>
            </tr>
         <?php endforeach; ?>
      </tbody>
   </table>

   <div class="go-back">
      <a class="button is-danger go-back-link" href="index.php">Zpět</a>
   </div>
<?php endif; ?>