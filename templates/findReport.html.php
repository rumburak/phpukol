<h1 class="title is-h1 is-center">Vyhledat Reporty Zaměstnance</h1>
<form action="" method="post">
   <div class="field">
      <label class="label" for="zamestnanec">Vyberte zaměstnance</label>
      <select class="select" name="zamestnanec" id="zamestnanec">
         <?php foreach ($employees as $employee) : ?>
            <option value="<?= $employee['id'] ?>"> <?= $employee['jmeno'] . " " . $employee['prijmeni'] ?> </option>
         <?php endforeach; ?>
      </select>
   </div>
   <input class="button is-primary" type="submit" value="Vyhledat">
</form>