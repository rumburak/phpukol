<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="./bulma.css">
    <?php if (isset($css)) : ?>
        <link rel="stylesheet" href="<?= $css ?>">
    <?php endif ?>
    <script src="https://kit.fontawesome.com/bce601a6a8.js" crossorigin="anonymous"></script>
</head>

<body>

    <!-- <nav>
        <ul>
            <?php if ($isLoggedIn) : ?>
                <li><a href="index.php">Domů</a></li>
                <li><a href="index.php?route=auth/logout">Odhlásit</a></li>
                <?php if ($user["pozice"] == "admin") : ?>
                    <li><a href="index.php?route=employee/addNew">Nový zaměstnanec</a></li>
                <?php endif; ?>
                <li><a href="index.php?route=employee/changePassword">Změna Hesla</a></li>
            <?php else : ?>
                <li><a href="index.php?route=auth/login">Přihlásit se</a></li>
                <li><a href="index.php">Domů</a></li>
            <?php endif ?>
        </ul>
    </nav> -->

    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="https://bulma.io">
                <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
            </a>

            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a href="index.php" class="navbar-item">
                    Domů
                </a>
                <?php if ($isLoggedIn) : ?>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Možnosti
                        </a>

                        <div class="navbar-dropdown">

                            <?php if ($user["pozice"] == "admin") : ?>
                                <a href="index.php?route=admin/importEmployee" class="navbar-item">
                                    Import Zaměstnanců
                                </a>
                            <?php endif ?>

                            <?php if ($user["pozice"] == "mistr") : ?>
                                <a href="index.php?route=employee/newTask" class="navbar-item">
                                    Založit Nový Úkol
                                </a>
                            <?php endif ?>

                            <?php if ($user["pozice"] == "admin" || $user["pozice"] == "mistr") : ?>
                                <a href="index.php?route=findReport" class="navbar-item">
                                    Pracovní Reporty
                                </a>
                            <?php endif ?>

                            <?php if ($user["pozice"] == "dělník") : ?>
                                <a href="index.php?route=employee/taskList" class="navbar-item">
                                    Vaše Práce
                                </a>
                            <?php endif ?>
                            <?php if ($user["pozice"] == "dělník") : ?>
                                <a href="index.php?route=employee/chooseTask" class="navbar-item">
                                    Volba Práce
                                </a>
                            <?php endif ?>
                            <a href="index.php?route=employee/changePassword" class="navbar-item">
                                Změna hesla
                            </a>
                            <a href="index.php?route=employee/edit&id=<?= $user['id'] ?>" class="navbar-item">
                                Úprava osobních údajů
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item">
                                Další možnosti
                            </a>
                        </div>
                    </div>
                <?php endif ?>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                        <?php if ($isLoggedIn && $user["pozice"] == "admin") : ?>
                            <a href="index.php?route=employee/addNew" class="button is-primary">
                                <strong>Nový Zaměstnanec</strong>
                            </a>
                        <?php endif ?>
                        <?php if ($isLoggedIn) : ?>
                            <a href="index.php?route=auth/logout" class="button is-danger">
                                Odhlásit
                            </a>
                        <?php else : ?>
                            <a href="index.php?route=auth/login" class="button is-success">
                                Přihlásit se
                            </a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <main class="container">
        <?= $output ?>
    </main>
</body>

</html>