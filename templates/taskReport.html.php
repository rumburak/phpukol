<?php if ($hasPermission == true) : ?>
   <h1 class="title is-h1">Výkaz práce</h1>
   <?php if (isset($messages)) : ?>
      <ul>
         <?php foreach ($messages as $message) : ?>
            <li><?= $message ?></li>
         <?php endforeach ?>
      </ul>
   <?php endif ?>
   <form method="POST">
      <div class="field">
         <label class="label" for="poznamka">Poznámka</label>
         <textarea class="textarea" name="poznamka" id="poznamka" cols="30" rows="6"></textarea>
      </div>
      <div class="field">
         <label class="label" for="hodiny">Počet Hodin</label>
         <input type="number" name="pocet_hodin" min="1" id="hodiny">
      </div>
      <input type="hidden" name="ukol_id" value="<?= $task['ukol_id'] ?? $task['id']  ?>">
      <a class=" mt-1 mr-1 button is-danger" href="index.php">Zpět</a>
      <input class="button is-link" type="submit" value="Vykázat">
   </form>
<?php else : ?>
   <h1>Nemůžete Vykázat na nepřidělený úkol</h1>
<?php endif; ?>